import React from 'react';
import { View, Text, Button } from 'react-native';
import { Actions } from 'react-native-router-flux';
//Actions.propertyView({ id: property.pId })
class PropertyView extends React.Component {
    render(){
        return (
            <View>
                <Button
                    title="View Map"
                    onPress={() => Actions.mapView()}
                />
            </View>
        );
    }
}

export default PropertyView;