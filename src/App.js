import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native';

import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
import { Scene, Router } from 'react-native-router-flux'
import PropertyView from './components/PropertyView'
import Map from './components/map'

const App: () => React$Node = () => {
  return (
    <Router 
    backButtonTintColor="#ffffff"
    backButtonTextStyle={{ color: '#ffffff' }}
    titleStyle={{ color: '#ffffff', fontWeight: '200' }} 
    navigationBarStyle={{ backgroundColor: '#3f51b5' }}>
    <Scene key="root" hideNavBar>{/* Hide extra header due to nested scenes */}
        {/* Tab Container */}
        <Scene key="tabbar" tabs activeBackgroundColor="#808080" activeTintColor="#ffffff">



            {/* Tab and it's scenes */}
            <Scene title="PROPERTIES" key="properties">
                <Scene initial key="propertyView" component={PropertyView} title="Property View" />
                <Scene hideTabBar key="mapView" component={Map} title="Map" />
            </Scene>

            
        </Scene>
    </Scene>
</Router>
  );
};

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: Colors.lighter,
  },
  engine: {
    position: 'absolute',
    right: 0,
  },
  body: {
    backgroundColor: Colors.white,
  },
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
    color: Colors.black,
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
    color: Colors.dark,
  },
  highlight: {
    fontWeight: '700',
  },
  footer: {
    color: Colors.dark,
    fontSize: 12,
    fontWeight: '600',
    padding: 4,
    paddingRight: 12,
    textAlign: 'right',
  },
});

export default App;
