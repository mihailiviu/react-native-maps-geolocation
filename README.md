Example react native app that crashes when react-native-maps and react-native-geolocation-service modules are installed at the same time.

Once react-native-geolocation-service module is removed, the app works fine!

react-native-geolocation-service module is needed to get lat and long of users phone and is more accurate than using the javascript navigator object!

This was tested on Windows 10 using Android Studio emulators:

Nexus 5 API 23
Pixel API 27
Pixel 2 API 28